import sublime, sublime_plugin
from sublime import Region
import subprocess
from collections import OrderedDict

current_line_number = None

view = None

class hook_infoCommand(sublime_plugin.TextCommand):


	def run(self, edit):
		current_file = self.view.file_name()
		lint_format = ".less"
		
		# Grab the current line-number so that we can change the 
		# variable on this line specifically
		global view
		view = self.view
		line = view.line(view.sel()[0].b)
		(row, col) = view.rowcol(line.begin())
		global current_line_number
		current_line_number = row + 1

		# We first check to see whether the current file format
		# matches a CSS file. If it does, the command will run.
		if current_file.endswith(lint_format):
			class_dictionary = line_split.run(self, view)
			self.async_popup(get_hook(class_dictionary))

	def async_popup(self, hook_list):

		if hook_list[0] == None:
			
			popup = """
				<style>html,body{{margin: 0; padding: 5px;}}</style>
				<span>No hook found.</span>"""

		else:

			popup = """
				<style>html,body{{margin: 0; padding: 5px;}} h1{{padding: 0; margin: 0;}} </style>
				<h1>{}</h1><span>Start line: {} <br> End line: {}</span>""".format(hook_list[0], hook_list[1], hook_list[2])

		view.show_popup(popup)

class line_split:

	def __init__(self, view):
		self.view		= view

	def run(self, view):
		document = view.substr(sublime.Region(0, view.size()))
		lines = document.splitlines()
		class_dictionary = OrderedDict()

		for index, line in enumerate(lines, 1):
			class_dictionary[index] = line

		class_dictionary = strip_comments(class_dictionary)

		return class_dictionary

def get_hook(dictionary):
	group_count = -1;
	responsive_hook = "hook-responsive-"

	is_hook = False
	hook_found = False

	dictionary = strip_comments(dictionary)

	for line in dictionary.items():
		line_number = line[0]
		class_name = line[1]

		if not is_hook and "{" in class_name:

			if responsive_hook in class_name:

				# if line[0] is after current_line_number, end function / break
				if current_line_number >= line_number:
					is_hook = True
					hook_type = class_name.split("(")[0].replace(".", "").replace("#", "")
					start_line = line_number

		if is_hook:

			if "{" in class_name:
			# We increase the group count AFTER, so that we don't grab the
			# wrong a line that doesn't exist from the dictionary
				group_count += 1

			if "}" in class_name:
			# We lower the group count AFTER, so that we don't grab the
			# wrong a line that doesn't exist from the dictionary
				group_count -= 1

			# When group_count = -1 , check to see if this value is greater than your line number.
			if group_count == -1:
				end_line = line_number

				if end_line < current_line_number:
					is_hook = False

				else:
					sublime.status_message("   {} found at line {} finishing at line {}".format(hook_type, start_line, end_line))
					is_hook = False
					hook_found = True
					return [hook_type, start_line, end_line]

	if not hook_found:
		hook_type = None
		sublime.status_message("  No hook found")
		return [hook_type, 0, 0]

def strip_comments(dictionary):

	stripped_dictionary = OrderedDict()
	add_to_dic = True

	for line in dictionary.items():
		line_number = line[0]
		class_name = line[1].strip()

		if add_to_dic:

			if class_name.startswith("/*"):

				if "*/" in class_name:
					continue

				else:
					add_to_dic = False

			if class_name.startswith("//"):
				continue
				
			elif add_to_dic:
				stripped_dictionary[line_number] = class_name

		else:

			if "*/" in class_name:
				add_to_dic = True
				continue

	return stripped_dictionary